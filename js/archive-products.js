(function(){
    'use strict';

    var productsCountPerPage = 12;

    function productsPagination(productAlbumId, productSet, totalItems){
        $.getJSON( "https://jsonplaceholder.typicode.com/photos", function( data ) {
            var pagination = $('#product-pagination'),
                paginationItems = '',
                isActive = '',
                pageCount = 0;

            if(totalItems < productsCountPerPage ){
                return false;
            }

            pageCount = Math.ceil(totalItems / productsCountPerPage);

            //create pagination items depending on pageCount;
            for (var i = 1; i <= pageCount; i++) {
                if(productSet != i){
                    isActive = '';
                }else{
                    isActive = 'class="active"';
                }
                paginationItems += '<li '+ isActive +'><a href="#" data-product-album-id="' + productAlbumId + '" data-product-set="' + i + '">' + i + '</a></li>';
            }

            pagination.html(paginationItems);
        });
    }

    function getProducts( productAlbumId, productSet){
        $.getJSON( "https://jsonplaceholder.typicode.com/photos", function( data ) {
            var products = [],
                productsList = '',
                totalItems = 0,
                counter = 0;
            
            //get the products
            $.each( data, function( key, value ) {
                if(value.albumId === productAlbumId){
                    counter++;
                    value.index = counter;
                    products.push(value);
                }
            });

            totalItems = products.length;

            // calculate start and end item indexes
            var startIndex = (productSet - 1) * productsCountPerPage,
                endIndex = Math.min(startIndex + productsCountPerPage - 1, totalItems - 1);

            if(startIndex == 0){
                startIndex++;
                endIndex++;
            }

            $.each( products, function( key, value ) {
                if(value.albumId === productAlbumId){
                    if(value.index >= startIndex && value.index <= endIndex){
                        productsList +=
                            '<div class="col-md-3 col-sm-6">' +
                                '<figure class="card">' +
                                    '<a href="single-product.html#' + value.id + '">' +
                                    '<img src="' + value.thumbnailUrl + '" alt="' + value.title + '" class="card__img">' +
                                    '</a>' +
                                    '<figcaption class="card__title">' + value.title + '</figcaption>' +
                                '</figure>' +
                            '</div>';
                    }
                }
            });
            productsPagination(productAlbumId, productSet, totalItems);

            $('#product-list').html(productsList);
        });
    }

    function searchProduct(productSearch){
        getProducts(productSearch, 1);
    }

    $(document).ready(function(){
        
        //productAlbumId
        //productSet
        getProducts( 1, 1);

        //Attach on click events on paginations links
        $('body').on('click', '#product-pagination a', function(){
            var productSet = $(this).attr('data-product-set'),
                productAlbumId = $(this).attr('data-product-album-id');
            getProducts( parseInt(productAlbumId), parseInt(productSet));
        });

        $('#search-product-form').on('submit', function(e){
            e.preventDefault();
            var formData = $('#product-search').val();
            searchProduct(parseInt(formData));
        });
    });

}());