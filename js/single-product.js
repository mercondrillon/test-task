(function(){
    'use strict';

    var productsCountPerPage = 12;

    function getProduct( productId){
        
        $.getJSON( "https://jsonplaceholder.typicode.com/photos", function( data ) {
            var productDetails = '';
            //get the products
            $.each( data, function( key, value ) {
                if(value.id === productId){
                    productDetails = 
                    '<img src="' + value.url + '" alt="' + value.title + '" class="card__img">' + 
                    '<figcaption class="card__title">' + value.title + '</figcaption>';
                }
            });

            $('#single-product').html(productDetails);
        });
    }

    $(document).ready(function(){
        if(window.location.hash){
            var hash = window.location.hash;
            hash = parseInt(hash.replace('#', ''));
            
            //productId
            getProduct(hash);
        }

    });

}());